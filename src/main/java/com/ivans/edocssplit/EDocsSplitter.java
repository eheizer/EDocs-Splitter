package com.ivans.edocssplit;

import java.io.File;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import com.ivans.batch.StatusCode;
import com.nxtech.batch.Archiver;
import com.nxtech.batch.BatchException;
import com.nxtech.batch.UnitOfWork;
import com.nxtech.core.common.CMConfiguration;
import com.nxtech.core.common.CMLogLevel;
import com.nxtech.core.common.CMLogManager;
import org.apache.commons.codec.binary.Base64;
import com.nxtech.util.io.FileUtils;

// This class is called by Processor (service orchestrator)
public class EDocsSplitter implements UnitOfWork {

	protected static StatusCode _result = StatusCode.Success; 

	protected static String _inputFileDir = "";
	protected static String _outputFileDir = "";
	protected static String _outputAttachmentsDir = "";
	protected static String _tempFileDir = "";	
	protected static String _failedDir = "";
	protected static boolean _cleanupFiles = false;
	private static String _archiveRoot;
	private static int _daysToSaveArchives;
	
	protected static String _newline = System.getProperty("line.separator");
	
	public EDocsSplitter() throws Exception {
		CMLogManager logger = CMLogManager.getInstance();
		CMConfiguration config = new CMConfiguration();
		String configFileName = CMConfiguration.getConfigFileName();
		String appRoot = CMConfiguration.getAppRoot();
		logger.log("EDocsSplitter", "EDocsSplitter", "Config file name:"+configFileName, CMLogLevel.DIAG);
		
		FileUtils fu = new FileUtils();
		String configStr = fu.readStringFromFile(CMConfiguration.getConfigRoot()+CMConfiguration.getConfigFileName());
		logger.log("EDocsSplitter", "EDocsSplitter", "Working with the following configuration:"+_newline+
				configStr, CMLogLevel.DIAG);

		logger.log("EDocsSplitter", "EDocsSplitter", "Getting XPath for agency code.", CMLogLevel.TRACE);
		_inputFileDir = config.getKeyValue(configFileName, "input", appRoot+"Input")+File.separator;
		_outputFileDir = config.getKeyValue(configFileName, "output", appRoot+"Output")+File.separator;
		_outputAttachmentsDir = config.getKeyValue(configFileName, "outputAttachments",_outputFileDir);
		if (! _outputAttachmentsDir.endsWith(File.separator))
			_outputAttachmentsDir+=File.separator;
		_failedDir = config.getKeyValue(configFileName, "failedDir", appRoot+"Failed");
		_tempFileDir = config.getKeyValue(configFileName,"tempDir", appRoot+"Temp")+File.separator;
		String cleanupFilesStr = config.getKeyValue(configFileName, "cleanupFiles", "YES");
		_cleanupFiles = cleanupFilesStr.toUpperCase().equals("YES");

		_archiveRoot = config.getKeyValue(configFileName, "archiveRoot","");
		if (_archiveRoot.equals("")) {
			logger.log("EDocsSplitter", "EDocsSplitter", 
					"No archiveRoot configured. Will not archive files.", CMLogLevel.DIAG);
		}
		else {
			String daysToSaveArchiveStr = config.getKeyValue(configFileName, "daysToSaveArchive", "30");
			_daysToSaveArchives = Integer.parseInt(daysToSaveArchiveStr); 
			logger.log("EDocsSplitter", "EDocsSplitter", "ArchiveRoot:"+_archiveRoot, CMLogLevel.DIAG);
			logger.log("EDocsSplitter", "EDocsSplitter", "Will save archives for:"+_daysToSaveArchives+" days.", CMLogLevel.DIAG);
		}
		StringBuffer state = new StringBuffer();
		state.append( "inputFileDir= "+_inputFileDir+_newline);
		state.append( "outputFileDir= "+_outputFileDir+_newline);
		state.append( "outputAttachmentsDir= "+_outputAttachmentsDir+_newline);
		state.append( "failedDir= "+_failedDir+_newline);
		state.append( "tempDir= "+_tempFileDir+_newline);
		state.append( "cleanupFiles= "+_cleanupFiles+_newline);
		logger.log("EDocsSplitter", "EDocsSplitter",state.toString(), CMLogLevel.DIAG);
		checkFailedDirExistence();
	}

	private void checkFailedDirExistence() {
		CMLogManager logger = CMLogManager.getInstance();
		File failedDir = new File(_failedDir);
		if (!failedDir.exists()) {
			logger.log("Processor", "checkFailedDirExistence", "Creating "+_failedDir,CMLogLevel.DIAG);
			boolean createdOK = failedDir.mkdirs();
			if (!createdOK) logger.log("Processor", "checkFailedDirExistence", 
					"Could not create Failed directory.",CMLogLevel.WARN);
		}
	}

	public EDocsSplitter (String bogus) {}
	
	// TBD - Check if XML file name already exists
	public String execute(String xml, String appHome) throws BatchException {
		// Get all files in the input directory
		CMLogManager logger = CMLogManager.getInstance();
		FileUtils fu = new FileUtils();
		try {
			if (!_archiveRoot.equals("")) {
				try {
					Archiver archiver = new Archiver(_archiveRoot);
					archiver.saveBySubstring(_inputFileDir, "", false, true);
					archiver.purge(_daysToSaveArchives);
				}
				catch (Throwable t) {
					String errMsg = "Could not archive Send directory. Error is:"+t.toString();
					logger.log("EDocsSplitter", "execute", errMsg, CMLogLevel.ERROR);
					_result = StatusCode.InternalError;
				}
			}

			// TBD - ARE there any?
			logger.log("EDocsSplitter", "execute", "Deleting all files from Temp Directory", CMLogLevel.TRACE);
			FileUtils.clearDir(_tempFileDir);
			Vector<String> files = fu.getFileListAbsolute(_inputFileDir, "");
			logger.log("EDocsSplitter", "execute", "Number of files in "+
					_inputFileDir+":"+files.size(), CMLogLevel.DIAG);
			String relativeFileName = "";
			for (String fullFileName:files) {
				try {
					logger.log("EDocsSplitter", "execute", "Splitting "+fullFileName, CMLogLevel.DIAG);
					splitEDocFile(fullFileName);
				}
				catch (Throwable t) {
					logger.log("EDocsSplitter", "execute", "Unexpected error processing file "+
							fullFileName+" "+t.toString(), CMLogLevel.ERROR);
					_result=StatusCode.InternalError;
					File f = new File(fullFileName);
					if (f.exists()) // Hasn't already been moved.
						relativeFileName = FileUtils.getFileName(fullFileName);
						FileUtils.renameFile(fullFileName, _failedDir+File.separator+relativeFileName);
				}
			}
			if (_cleanupFiles) {
				cleanUp(files);
			}
			// Package up the Failed files for the run.
			Archiver failedArchiver = new Archiver(_failedDir);
			failedArchiver.saveBySubstring(_failedDir,"",true);
		}
		catch (Throwable t) {
			logger.log("EDocsSplitter", "execute", "Error in execution:"+
					t.toString(), CMLogLevel.ERROR);
			_result = StatusCode.InternalError;
		}

		// By contract with the Processor, return a string representation of the error number.
		return Integer.toString(_result.ordinal()); 
	}
	
	private void splitEDocFile(String fullFileName) throws Exception {
		CMLogManager logger = CMLogManager.getInstance();
		logger.log("EDocsSplitter", "splitEDocFile", "Entered", CMLogLevel.TRACE);
		FileUtils fu = new FileUtils();
		String content = fu.readStringFromFile(fullFileName);
		// Favor CRLF
		String newline = "\r\n";
		if (!content.contains(newline)) newline = "\n";
		logger.log("EDocsSplitter", "splitEDocFile", "Using newline with length:"+newline.length(), CMLogLevel.DIAG);
		
		// Have some EDocs where the newlines are inconsistent. Normalize to a single line terminator.
		content = normalizeNewlines(content);
		
		String boundary = getBoundary(content, newline);
		if (!boundary.equals("")) {
			logger.log("EDocsSplitter", "splitEDocFile", "Using boundary "+boundary, CMLogLevel.DIAG);
			String xmlPart = getXMLPart(content, boundary);
			String xmlPartName = getXMLPartFileName(fullFileName); 
			fu.writeStringToFile(_outputFileDir+xmlPartName, xmlPart, false); // Don't append
			pullAndWriteAttachments(content, boundary, newline);
		}
		else {
			logger.log("EDocsSplitter", "splitEDocFile", fullFileName+
					" is not a MIME document. Writing XML without splitting.", CMLogLevel.DIAG);
			String xmlPart = fu.readStringFromFile(fullFileName);
			String xmlPartName = getXMLPartFileName(fullFileName); 
			fu.writeStringToFile(_outputFileDir+xmlPartName, xmlPart, false); // Don't append
		}
		// HashMap<String,String> attachmentParts = getAttachmentParts(content, boundary, newline);
		// writeAttachmentParts(attachmentParts);
	}
	
	private String normalizeNewlines(String content) {
		CMLogManager logger = CMLogManager.getInstance();
		logger.log("EDocsSplitter", "normalizeNewlines", "Entered", CMLogLevel.TRACE);
		StringBuilder sb = new StringBuilder(content);
		// int lastIndexToCheck = sb.indexOf("Content-ID:");
		int lastIndexToCheck = sb.indexOf("ACORD");
		if (lastIndexToCheck>=0) {
			int fromIndex = 0;
			boolean haveLFBeforeCID = true;
			while (haveLFBeforeCID) {
				int lfIndex = sb.indexOf("\n",fromIndex);
				if (lfIndex>0 && lfIndex<lastIndexToCheck) {
					if (!sb.substring(lfIndex-1,lfIndex).equals("\r")) {
						sb.insert(lfIndex,"\r");
						fromIndex = lfIndex+2;
					}
					else {
						fromIndex = lfIndex+1;
					}
				}
				else {
					haveLFBeforeCID = false;
				}
			}
		}
		return sb.toString();
	}
	
	private String getXMLPart(String content, String boundary) {
		CMLogManager logger = CMLogManager.getInstance();
		logger.log("EDocsSplitter", "getXMLPart", "Entered", CMLogLevel.TRACE);
		String xmlPart = "";
		int indexToFirstChar = content.indexOf("<?xml");
		if (indexToFirstChar<0) indexToFirstChar = content.indexOf("<ACORD");
		int indexToNextBoundary = content.indexOf(boundary, indexToFirstChar);
		xmlPart = content.substring(indexToFirstChar, indexToNextBoundary).trim();
		return xmlPart;
	}
	
	private String getBoundary(String content, String newline) {
		CMLogManager logger = CMLogManager.getInstance();
		logger.log("EDocsSplitter", "getBoundary", "Entered", CMLogLevel.TRACE);
		String boundary = "";
		String boundaryMarker = "boundary=";
		int indexToStart = content.indexOf(boundaryMarker);
		if (indexToStart>=0) {
			indexToStart+=boundaryMarker.length();
			int indexToEnd = content.indexOf(newline, indexToStart);
			if (indexToEnd>=0) {
				boundary = content.substring(indexToStart,indexToEnd);
				boundary = boundary.replaceFirst("\"", "--");
				boundary = boundary.replace("\"", "");
			}
			else {
				logger.log("EDocsSplitter", "getBoundary", "Could not find newline in file", CMLogLevel.WARN);
			}
		}
		else {
			logger.log("EDocsSplitter", "getBoundary", "Could not find MIME boundary in file", CMLogLevel.WARN);
		}
		return boundary;
	}
	
	private String getXMLPartFileName(String fullFileName) {
		CMLogManager logger = CMLogManager.getInstance();
		logger.log("EDocsSplitter", "getXMLPartFileName", "Entered", CMLogLevel.TRACE);
		String xmlPartFileName = "";
		String extension = FileUtils.getFileExtension(fullFileName);
		if (!extension.toUpperCase().equals(".XML")) fullFileName = fullFileName+".xml";
		xmlPartFileName = FileUtils.getFileName(fullFileName);
		return xmlPartFileName;
	}
	
	private void pullAndWriteAttachments(String content, String boundary, String newline)
		throws Exception
	{
		CMLogManager logger = CMLogManager.getInstance();
		logger.log("EDocsSplitter", "getAttachmentParts", "Entered", CMLogLevel.TRACE);
		HashMap<String,String> attachmentParts = new HashMap<String,String>();
		int indexToFirstBoundary = content.indexOf(boundary); // This is to the XML
		int indexToStartBoundary = content.indexOf(boundary, indexToFirstBoundary+1);
		int attachmentNum = 0;
		while (indexToStartBoundary>=0) {
			attachmentNum+=1;
			logger.log("EDocsSplitter", "getAttachmentParts", "Splitting attachment "+attachmentNum, CMLogLevel.DIAG);
			int indexToEndBoundary = content.indexOf(boundary, indexToStartBoundary+1);
			if (indexToEndBoundary<0) break;
			String attachmentStr = content.substring(indexToStartBoundary, indexToEndBoundary).trim();
			Attachment attachment = new Attachment(attachmentStr, newline);
			String attachmentFileName = attachment.get_attachmentName();
			String attachmentContent = attachment.get_attachmentContent();
			// attachmentParts.put(attachmentFileName,attachmentContent);
			writeAttachmentFile (attachmentFileName,attachmentContent);
			indexToStartBoundary = indexToEndBoundary;
		}
	}
	
	private void writeAttachmentFile(String fileName, String attachmentBase64) 
			throws Exception
	{
		CMLogManager logger = CMLogManager.getInstance();
		logger.log("EDocsSplitter", "writeAttachmentFile", "Entered", CMLogLevel.TRACE);
		FileUtils fu = new FileUtils();
		String fullFileName = _outputAttachmentsDir+fileName;
		logger.log("EDocsSplitter", "writeAttachmentFile", "Writing "+fullFileName, CMLogLevel.DIAG);
		fu.writeStringToFile(fullFileName, attachmentBase64,false);
	}
	
	private void cleanUp(Vector<String> fileList) {
		CMLogManager logger = CMLogManager.getInstance();
		try { 
			logger.log("EDocsSplitter", "cleanUp", "Deleting all files from Temp Directory", CMLogLevel.DIAG);
			FileUtils.clearDir(_tempFileDir);
			logger.log("EDocsSplitter", "cleanUp", "Deleting original EDocs files.", CMLogLevel.DIAG);
			for (String fileName:fileList) {
				FileUtils.deleteFile(fileName);
			}
		}
		catch (Throwable t) {
			String statusMsg = "Error in cleanup phase:"+t.toString();
			logger.log("EDocsSplitter","cleanUp",statusMsg, CMLogLevel.ERROR);
		}
	}
	
	// Convert to JUnit5 as part of project
	public static void main(String[] args) {
		String appHome = "C:\\Users\\Ron\\Dropbox\\PROFESSIONAL\\NxTech\\MyProductDoc\\Download\\EDocsSplitter\\Test\\";
		_inputFileDir = appHome+"Input\\";
		_outputFileDir = appHome+"Output\\";
		_tempFileDir = appHome+"Temp\\";
		EDocsSplitter es = new EDocsSplitter("Blah");
		try {
			es.splitEDocFile(_inputFileDir+"4294277_PolicyAlert.D082620.T124845.1.dat");
		}
		catch (Throwable t) {System.out.println(t.toString());}
		
	}

}
