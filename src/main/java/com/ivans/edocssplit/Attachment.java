package com.ivans.edocssplit;

import com.nxtech.core.common.CMLogLevel;
import com.nxtech.core.common.CMLogManager;

public class Attachment {

	private String contentMarker1 = "Content-id:";
	private String contentMarker2 = "Content-ID:";
	public Attachment(String attachmentStr, String newline) {
		CMLogManager logger = CMLogManager.getInstance();
		logger.log("Attachment", "Attachment", "Entered", CMLogLevel.TRACE);
		int indexToContentID = attachmentStr.indexOf(contentMarker1);
		if (indexToContentID>=0)
			indexToContentID+=contentMarker1.length();
		else {
			indexToContentID = attachmentStr.indexOf(contentMarker2)+contentMarker2.length();
		}
		int indexToNewline = attachmentStr.indexOf(newline,indexToContentID);
		_attachmentName = attachmentStr.substring(indexToContentID, indexToNewline).trim();
		int indexToStartOfAttachmentContent = attachmentStr.indexOf("JVBER");
		_attachmentContent = attachmentStr.substring(indexToStartOfAttachmentContent);
	}

	public String get_attachmentContent() {
		return _attachmentContent;
	}


	public String get_attachmentName() {
		return _attachmentName;
	}

	private String _attachmentContent;
	private String _attachmentName;
	
}
