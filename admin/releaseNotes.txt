EDocsSplit.jar Release Notes

Spec 8.6.1 Build 1 - 06/07/2021
- Don't archive if archiveRoot isn't configured, rather than defaulting 
to a particular directory.
- Log the configuration file that was used.

Spec 5.03 Build 1 - 03/17/2021
- Handle EDocs where line termination characters are inconsistent (some LF, some CRLF).
- Pass through non-MIME-encoded docs.

Spec 5.02 Build 1 - 10/27/2020
- Allow separate configuration for output directory for attachments (<outputAttachments>).
If not present, default to directory for output XML files.

Spec 5.01 Build 1 - 10/01/2020
- Initial release.




